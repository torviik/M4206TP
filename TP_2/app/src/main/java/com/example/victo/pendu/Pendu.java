package com.example.victo.pendu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class Pendu extends AppCompatActivity implements View.OnClickListener
{

    private Button mButton;
    private TextView mTextView;
    private EditText mEditText;
    private ImageView mImageView;
    private int n ;
    private String[] mlisteMots;
    private String[] reponse;
    private String mstring ;
    private int image[] = {R.drawable.pendu_tp_0,R.drawable.pendu_tp_1,R.drawable.pendu_tp_2,R.drawable.pendu_tp_3,R.drawable.pendu_tp_4,R.drawable.pendu_tp_5};
    private int mNbFaute = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pendu);

        mButton = (Button) findViewById(R.id.myButton);
        mEditText = (EditText) findViewById(R.id.myeditText);
        mTextView = (TextView) findViewById(R.id.myTextView);
        mImageView = (ImageView) findViewById(R.id.myimageView);

        Random rand = new Random();
        n = rand.nextInt(7) + 1;

        switch (n)
        {
            case 1 : mlisteMots = new String[] {"lys"};
                break;
            case 2 : mlisteMots = new String[] {"maison"};
                break;
            case 3 : mlisteMots = new String[] {"rue"};
                break;
            case 4 : mlisteMots = new String[] {"Sac"};
                break;
            case 5 : mlisteMots = new String[] {"Poisson"};
                break;
            case 6 : mlisteMots = new String[] {"Chat"};
                break;
            case 7 : mlisteMots = new String[] {"Savate"};
                break;
        }

        reponse = new String[mlisteMots[0].length()];
        mstring = "";
        for (int b = 0 ; b < mlisteMots[0].length(); b++)
        {
            reponse[b] = "_ ";
            mstring = mstring + reponse[b];
        }
        mTextView.setText("" + mstring);
        mButton.setOnClickListener(this);

        mImageView.setImageDrawable(getResources().getDrawable(image[0]));


    }

    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.myButton:
                verification();
                break;
            default:
        }
    }

    public void verification()
    {
        String mLettre;
        mstring="";
        mLettre = String.valueOf(mEditText.getText());
        for (int i = 0 ; i < mlisteMots[0].length(); i++)
        {
                if (mlisteMots[0].charAt(i) == mLettre.charAt(0))
                {
                    reponse[i] = mLettre;
                    mstring = mstring + reponse[i];
                }
                else
                {
                    mstring = mstring + reponse[i];
                }
                mTextView.setText("" + mstring);
        }

        if (!mlisteMots[0].contains(mLettre)){
            mNbFaute++;
            mImageView.setImageDrawable(getResources().getDrawable(image[mNbFaute]));
        }

    }
}
